package main

import (
	"fmt"
	"github.com/nfnt/resize"
	"image/jpeg"
	"image/png"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
)

func main() {
	if len(os.Args) != 5 {
		fmt.Println("Usage:")
		fmt.Println("\tresizePic path [0|1] maxWidth maxHeight")
		fmt.Println("\tpresent 1 if you want walk through the path recursively, 0 otherwise.")
	} else {
		recursive, err1 := strconv.ParseUint(os.Args[2], 10, 0)
		maxWidth, err2 := strconv.ParseUint(os.Args[3], 10, 0)
		maxHeight, err3 := strconv.ParseUint(os.Args[4], 10, 0)
		if err1 != nil || err2 != nil || err3 != nil {
			fmt.Println("Invalid argument.")
			return
		}
		thumbnailPatch(os.Args[1], uint(recursive), uint(maxWidth), uint(maxHeight))
	}
}

func thumbnailPatch(path string, recursive, maxWidth, maxHeight uint) bool {
	if recursive == 0 {
		list, _ := ioutil.ReadDir(path)
		for _, file := range list {
			tn := filepath.Join(path, "thumbnail", file.Name())
			_, err := os.Stat(tn)
			if os.IsNotExist(err) {
				thumbnail(filepath.Join(path, file.Name()), maxWidth, maxHeight)
			} else {
				fmt.Println("Thumbnail of", tn, "already exists, skipping...")
			}
		}
	} else if recursive == 1 {
		list := []string{}
		filepath.Walk(path, func(p string, f os.FileInfo, err error) error {
			list = append(list, p)
			return nil
		})
		for _, file := range list {
			dir, fileName := filepath.Split(file)
			tn := filepath.Join(dir, "thumbnail", fileName)
			_, err := os.Stat(tn)
			if !strings.Contains(file, "thumbnail") && os.IsNotExist(err) {
				thumbnail(file, maxWidth, maxHeight)
			} else if !os.IsNotExist(err) {
				fmt.Println("Thumbnail of", tn, "already exists, skipping...")
			}

		}
	}
	return true
}

func resizePic(name string) bool {
	ext := filepath.Ext(name)
	if strings.ToLower(ext) == ".png" {
		return resizePng(name)
	} else if strings.ToLower(ext) == ".jpg" {
		return resizeJpg(name)
	} else {
		fmt.Println("Unsupported file type:", name)
		return false
	}
}

func resizePng(name string) bool {
	file, err := os.Open(name)
	if err != nil {
		log.Fatal(err)
	}
	img, err := png.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()
	m := resize.Resize(1000, 0, img, resize.Lanczos3)
	dir, newName := filepath.Split(name)
	newDir := filepath.Join(dir, "resized")
	os.Mkdir(newDir, 0777)
	p := filepath.Join(newDir, newName)
	out, err := os.Create(p)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()
	png.Encode(out, m)
	return true
}

func resizeJpg(name string) bool {
	file, err := os.Open(name)
	if err != nil {
		log.Fatal(err)
	}
	img, err := jpeg.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	file.Close()
	m := resize.Resize(1000, 0, img, resize.Lanczos3)
	dir, newName := filepath.Split(name)
	newDir := filepath.Join(dir, "resized")
	os.Mkdir(newDir, 0777)
	p := filepath.Join(newDir, newName)
	out, err := os.Create(p)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()
	jpeg.Encode(out, m, nil)
	return true
}

func thumbnail(name string, maxWidth, maxHeight uint) bool {
	ext := filepath.Ext(name)
	if strings.ToLower(ext) == ".jpg" {
		return thumbnailJpg(name, maxWidth, maxHeight)
	} else if strings.ToLower(ext) == ".png" {
		return thumbnailPng(name, maxWidth, maxHeight)
	} else {
		fmt.Println("Unsupported format:", name)
		return false
	}
}

func thumbnailJpg(name string, maxWidth, maxHeight uint) bool {
	file, err := os.Open(name)
	if err != nil {
		log.Fatal(err)
	}
	img, err := jpeg.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	m := resize.Thumbnail(maxWidth, maxHeight, img, resize.Lanczos3)
	dir, newName := filepath.Split(name)
	newDir := filepath.Join(dir, "thumbnail")
	os.Mkdir(newDir, 0777)
	p := filepath.Join(newDir, newName)
	out, err := os.Create(p)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()
	jpeg.Encode(out, m, nil)
	return true

}
func thumbnailPng(name string, maxWidth, maxHeight uint) bool {
	file, err := os.Open(name)
	if err != nil {
		log.Fatal(err)
	}
	img, err := png.Decode(file)
	if err != nil {
		log.Fatal(err)
	}
	m := resize.Thumbnail(maxWidth, maxHeight, img, resize.Lanczos3)
	dir, newName := filepath.Split(name)
	newDir := filepath.Join(dir, "thumbnail")
	os.Mkdir(newDir, 0777)
	p := filepath.Join(newDir, newName)
	out, err := os.Create(p)
	if err != nil {
		log.Fatal(err)
	}
	defer out.Close()

	png.Encode(out, m)
	return true

}
